package scaffolder

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gopkg.in/yaml.v3"
	"os"
	"testing"
)

const scaffoldingyml = "./test/scaffolding.yml"

func TestUnmarshal(t *testing.T) {
	b, err := os.ReadFile(scaffoldingyml)

	assert.NoError(t, err)
	var s Scaffolding
	assert.NoError(t, Unmarshal(b, &s))
	assert.Len(t, s.Scaffolding.Files, 1)
	assert.Len(t, s.Scaffolding.SubDirectories, 1)
}

func TestCreateScaffolding(t *testing.T) {
	b, err := os.ReadFile(scaffoldingyml)

	assert.NoError(t, err)

	var s Scaffolding
	assert.NoError(t, yaml.Unmarshal(b, &s))
	p := fmt.Sprintf("./test/%s", s.Scaffolding.Name)
	assert.NoError(t, CreateScaffolding("./test", &s))
	assert.DirExists(t, p)
	assert.FileExists(t, fmt.Sprintf("%s/%s", p, s.Scaffolding.Files[0].Name))
	assert.NoError(t, os.RemoveAll("./test/testAbc"))
}
