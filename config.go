package scaffolder

import "os"

type ReadSource func(string) ([]byte, error)

var readSource ReadSource

func SetReadSource(r ReadSource) {
	readSource = r
}

func init() {
	SetReadSource(func(s string) ([]byte, error) {
		return os.ReadFile(s)
	})
}
