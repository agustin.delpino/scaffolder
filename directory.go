package scaffolder

import (
	"fmt"
	"os"
)

type Directory struct {
	Name           string       `yaml:"name"`
	Files          []*File      `yaml:"files"`
	SubDirectories []*Directory `yaml:"sub-directories"`
}

// AddFile adds a new File to the Directory
func (d *Directory) AddFile(f *File) *Directory {
	d.Files = append(d.Files, f)
	return d
}

// AddSubDir add a new Directory as subdirectory to the Directory
func (d *Directory) AddSubDir(sd *Directory) *Directory {
	d.SubDirectories = append(d.SubDirectories, sd)
	return d
}

func NewDir(n string) *Directory {
	return &Directory{
		Name:           n,
		Files:          []*File{},
		SubDirectories: []*Directory{},
	}
}

// createDirectory creates recursive the Directory structure in the disk
func createDirectory(l string, d *Directory) error {
	p := fmt.Sprintf("%s/%s", l, d.Name)

	if err := os.MkdirAll(p, 0777); err != nil {
		return err
	}

	for _, f := range d.Files {
		if err := CreateFile(p, f); err != nil {
			return err
		}
	}

	for _, sd := range d.SubDirectories {
		if err := createDirectory(p, sd); err != nil {
			return err
		}
	}

	return nil
}
