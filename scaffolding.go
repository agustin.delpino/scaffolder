package scaffolder

import (
	"gopkg.in/yaml.v3"
)

type Scaffolding struct {
	Scaffolding *Directory `yaml:"scaffolding"`
}

func CreateScaffolding(l string, s *Scaffolding) error {
	return createDirectory(l, s.Scaffolding)
}

func Unmarshal(b []byte, s *Scaffolding) error {
	return yaml.Unmarshal(b, s)
}

func Marshal(b *Scaffolding) ([]byte, error) {
	return yaml.Marshal(b)
}
