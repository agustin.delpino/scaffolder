package scaffolder

import (
	"fmt"
	"os"
)

type FileReadableContent func(*File) ([]byte, bool)

type File struct {
	Name        string `yaml:"name"`
	Content     string `yaml:"content"`
	From        string `yaml:"from"`
	fileContent FileReadableContent
}

// StringContent set the File.Content property
func (f *File) StringContent(s string) *File {
	f.Content = s
	return f
}

// FileContent set a readable content to the File
func (f *File) FileContent(c FileReadableContent) *File {
	f.fileContent = c
	return f
}

// Read retrieves the content set by FileContent
func (f *File) Read() ([]byte, bool) {
	return f.fileContent(f)
}

func (f *File) UnmarshalYAML(u func(interface{}) error) error {
	type rawFile File
	if err := u((*rawFile)(f)); err != nil {
		return err
	}

	if f.From != "" {
		if b, err := readSource(f.From); err != nil {
			return fmt.Errorf("the %s file is missing", f.From)
		} else {
			f.Content = string(b)
		}
	}

	return nil
}

func NewFile(n string) *File {
	return &File{
		Name:        n,
		Content:     "",
		fileContent: nil,
	}
}

func CreateFile(l string, f *File) error {
	var c []byte

	if f.Content != "" {
		c = []byte(f.Content)
	} else if f.fileContent != nil {
		if b, ok := f.Read(); ok {
			c = b
		}
	}

	if err := os.WriteFile(fmt.Sprintf("%s/%s", l, f.Name), c, 0777); err != nil {
		return err
	}

	return nil
}
