package scaffolder

import (
	"github.com/stretchr/testify/assert"
	"gopkg.in/yaml.v3"
	"os"
	"testing"
)

const (
	testyml string = "./test/file.test.yml"
)

func TestFile_UnmarshalYAML(t *testing.T) {
	b, err := os.ReadFile(testyml)

	assert.NoError(t, err)

	var f File

	assert.NoError(t, yaml.Unmarshal(b, &f))
	assert.Equal(t, f.Content, "Hello World")
}
